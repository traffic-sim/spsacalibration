import sumolib
import numpy as np
import pandas as pd
import os
import re


def geh(m, c):
    """
    Error function for hourly traffic flow measures after Geoffrey E. Havers
    :param m:
    :param c:
    :return:
    """
    if m + c == 0:
        return 0
    else:
        return sumolib.statistics.geh(m, c)


def sqv(m, c):
    return sumolib.statistics.sqv(None, m, c)


def calculate_traffic_volume_difference(detectors_csv_out, traffic_count_dataset_fname):
    import ErrorUtils
    real_counts, sim_counts_df = ErrorUtils.get_real_and_sim_data(detectors_csv_out, traffic_count_dataset_fname)
    real_counts_mat = real_counts.to_numpy()
    sim_counts_mat = sim_counts_df.to_numpy()
    rows, cols = real_counts_mat.shape
    abs_diff = np.zeros((rows, cols))

    for i in range(rows):
        for j in range(cols):
            abs_diff[i][j] = abs(sim_counts_mat[i][j] - real_counts_mat[i][j])

    return pd.DataFrame(abs_diff).sum().sum()


def geh_to_file(out_fname, detectors_out_csv, traffic_count_dataset_fname):
    import ErrorUtils
    real_counts, sim_counts_df = ErrorUtils.get_real_and_sim_data(detectors_out_csv,
                                                                  traffic_count_dataset_fname)

    real_counts_mat = real_counts.to_numpy()
    sim_counts_mat = sim_counts_df.to_numpy()
    rows, cols = real_counts_mat.shape
    geh_mat = np.zeros((rows, cols))
    sqv_mat = np.zeros((rows, cols))
    # num_edges = cols
    for i in range(rows):
        for j in range(cols):
            geh_mat[i][j] = geh(sim_counts_mat[i][j], real_counts_mat[i][j])
            sqv_mat[i][j] = sqv(sim_counts_mat[i][j], real_counts_mat[i][j])
    geh_df = pd.DataFrame(geh_mat)
    geh_df.columns = real_counts.columns
    geh_df['begin'] = real_counts.index.to_list()
    geh_df.set_index('begin', inplace=True)

    sqv_df = pd.DataFrame(sqv_mat)
    sqv_df.columns = real_counts.columns
    sqv_df['begin'] = real_counts.index.to_list()
    sqv_df.set_index('begin', inplace=True)

    writer = pd.ExcelWriter(out_fname, mode="w")
    geh_df.to_excel(writer, sheet_name="GEH")
    sqv_df.to_excel(writer, sheet_name="SQV")
    writer.close()
    return geh_df, sqv_df


def write_geh_statistics_to_excel(output_file, df):
    if os.path.exists(output_file):
        writer = pd.ExcelWriter(output_file, mode="a", if_sheet_exists='replace')
    else:
        writer = pd.ExcelWriter(output_file, mode="w")

    df.to_excel(writer)
    writer.close()


def parse_geh(fname):
    pattern = re.compile("(.*):(.*) GEH<(.*) for (.*)%")
    match_dict = {}
    for i, line in enumerate(open(fname)):
        matches = pattern.findall(line)
        if matches:
            match_dict[eval(matches[0][0])] = eval(matches[0][3])
    return pd.DataFrame.from_dict([match_dict])


def get_geh_stats_for_files(conf_set: list):
    out_df = pd.DataFrame()

    for conf in conf_set:
        df = parse_geh(os.path.join(conf.out_folder, f'exp_log.txt'))
        df['name'] = conf.name
        out_df = pd.concat([out_df, df])

    out_df.set_index('name', inplace=True)
    return out_df
