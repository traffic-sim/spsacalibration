import os
import pandas as pd
import ErrorUtils


def concat_geh_results(base_output_path, sim_begin, sim_end, interval, out_fname="geh.train.xlsx", assemble_train=True):
    geh_concat = pd.DataFrame()
    ts = list(range(sim_begin, sim_end + 1, interval))
    for i in range(len(ts) - 1):
        begin = ts[i]
        end = ts[i + 1]
        base_path = os.path.join(base_output_path,
                                 f"{str(begin)}_{str(end)}",
                                 "_best_sim")
        geh_file = os.path.join(base_path, "geh.train.xlsx" if assemble_train else "geh.test.xlsx")
        geh_df = pd.read_excel(geh_file, sheet_name="GEH")
        geh_df = geh_df.set_index('begin').T
        geh_concat = pd.concat([geh_concat, geh_df], axis=1)
    geh_concat = geh_concat.T

    writer = pd.ExcelWriter(os.path.join(base_output_path, out_fname), mode="w")
    geh_concat.to_excel(writer, sheet_name="GEH")
    writer.close()


def concat_traffic_counts_results(base_output_path, sim_begin, sim_end, interval, assemble_train=True,
                                  out_fname="real_vs_sim_comparison.xlsx"):
    avg_traffic_df_concat = pd.DataFrame()
    ts = list(range(sim_begin, sim_end + 1, interval))
    sheet_name = "Average Traffic (Train set)" if assemble_train else "Average Traffic (Test set)"
    for i in range(len(ts) - 1):
        begin = ts[i]
        end = ts[i + 1]
        base_path = os.path.join(base_output_path,
                                 f"{str(begin)}_{str(end)}",
                                 "_best_sim")
        traffic_file = os.path.join(base_path, "real_vs_sim_comparison.xlsx")
        avg_traffic_df = pd.read_excel(traffic_file, sheet_name=sheet_name)
        avg_traffic_df = avg_traffic_df.set_index('ts_sumo').T
        avg_traffic_df_concat = pd.concat([avg_traffic_df_concat, avg_traffic_df], axis=1)
    avg_traffic_df_concat = avg_traffic_df_concat.T
    full_out_fname = os.path.join(base_output_path, out_fname)
    if os.path.exists(full_out_fname):
        writer = pd.ExcelWriter(full_out_fname, engine='openpyxl', mode='a', if_sheet_exists='replace')  # mode="w")
    else:
        writer = pd.ExcelWriter(full_out_fname, mode="w")
    avg_traffic_df_concat.to_excel(writer, sheet_name=sheet_name)
    writer.close()


def concat_detectors_results(base_output_path, sim_begin, sim_end, interval, real_count_dataset,
                             out_fname="det.out.xlsx",
                             assemble_train=True):
    sim_count_df = pd.DataFrame()
    ts = list(range(sim_begin, sim_end + 1, interval))
    for i in range(len(ts) - 1):
        begin = ts[i]
        end = ts[i + 1]
        base_path = os.path.join(base_output_path,
                                 f"{str(begin)}_{str(end)}",
                                 "_best_sim")
        det_file = os.path.join(base_path,
                                "detector.out.xlsx" if assemble_train else os.path.join("test/detector.out.xlsx"))
        det_out_df = pd.read_excel(det_file, sheet_name="Simulated")
        det_out_df = det_out_df.set_index('ts_sumo').T
        sim_count_df = pd.concat([sim_count_df, det_out_df], axis=1)
    sim_count_df = sim_count_df.T

    real_dataset = pd.read_csv(real_count_dataset, sep=";", index_col="ts", parse_dates=True)

    ts = list(real_dataset.index.unique())
    ts_sumo = [(ts_other - ts[0]).total_seconds() for ts_other in ts]
    real_dataset['ts_sumo'] = ts_sumo
    real_dataset.set_index('ts_sumo', inplace=True)
    real_counts_df = real_dataset.loc[sim_count_df.index.unique().tolist()]

    sim_osm_edges = set(sim_count_df.columns.unique())
    real_osm_edges = set(real_counts_df.columns.unique())

    common_edges = sim_osm_edges.intersection(real_osm_edges)
    sim_diff = sim_osm_edges.difference(common_edges)
    real_diff = real_osm_edges.difference(common_edges)

    real_counts_df.drop(list(sim_diff.union(real_diff)), axis=1, inplace=True, errors='ignore')
    sim_count_df.drop(list(sim_diff.union(real_diff)), axis=1, inplace=True, errors='ignore')

    diff = real_counts_df - sim_count_df
    diff_abs = real_counts_df.abs() - sim_count_df.abs()

    writer = pd.ExcelWriter(os.path.join(base_output_path, out_fname), mode="w")
    sim_count_df.to_excel(writer, sheet_name="Simulated")
    real_counts_df.to_excel(writer, sheet_name="Real")
    diff.to_excel(writer, sheet_name="Real-Sim")
    diff_abs.to_excel(writer, sheet_name="Abs Real-Sim")
    writer.close()


def concat_obj_fun_results(base_output_path, sim_begin, sim_end, interval, out_fname="obj_fun.xlsx"):
    obj_fun_concat = pd.DataFrame()
    ts = list(range(sim_begin, sim_end + 1, interval))
    for i in range(len(ts) - 1):
        begin = ts[i]
        end = ts[i + 1]
        base_path = os.path.join(base_output_path,
                                 f"{str(begin)}_{str(end)}")
        obj_fun_fname = os.path.join(base_path, "obj_function.csv")
        obj_fun_df = pd.read_csv(obj_fun_fname, header=None)
        obj_fun_df.rename(columns={0: f"{str(begin)}_{str(end)}"}, inplace=True, errors='raise')
        obj_fun_concat = pd.concat([obj_fun_concat, obj_fun_df], axis=1)

    std_df = obj_fun_concat.std(axis=1).to_numpy()
    avg_df = obj_fun_concat.mean(axis=1).to_numpy()
    y = pd.DataFrame(avg_df, columns=['avg'])
    y_upper = pd.DataFrame(avg_df + std_df, columns=['upper'])
    y_lower = pd.DataFrame(avg_df - std_df, columns=['lower'])
    resume_df = pd.concat([y_lower, y, y_upper], axis=1)

    writer = pd.ExcelWriter(os.path.join(base_output_path, out_fname), mode="w")
    obj_fun_concat.to_excel(writer, sheet_name="Obj fun")
    resume_df.to_excel(writer, sheet_name="resume")
    writer.close()


def assemble_results(base_path: str,
                     net_path: str,
                     sim_begin: int,
                     sim_end: int,
                     interval: int,
                     train_dataset_path: str,
                     test_dataset_path: str,
                     taz_path: str = None):
    concat_obj_fun_results(base_path, sim_begin, sim_end, interval)
    concat_geh_results(base_path, sim_begin, sim_end, interval, out_fname="geh.train.xlsx", assemble_train=True)
    concat_geh_results(base_path, sim_begin, sim_end, interval, out_fname="geh.test.xlsx", assemble_train=False)
    concat_traffic_counts_results(base_path, sim_begin, sim_end, interval, assemble_train=True)
    concat_traffic_counts_results(base_path, sim_begin, sim_end, interval, assemble_train=False)
    concat_detectors_results(base_path, sim_begin, sim_end, interval, train_dataset_path,
                             out_fname="det.out.train.xlsx",
                             assemble_train=True)
    concat_detectors_results(base_path, sim_begin, sim_end, interval, test_dataset_path, out_fname="det.out.test.xlsx",
                             assemble_train=False)

    pd_det_out = pd.DataFrame()
    if taz_path is not None:
        ts = list(range(sim_begin, sim_end + 1, interval))
        for i in range(len(ts) - 1):
            begin = ts[i]
            end = ts[i + 1]
            exp_folder = os.path.join(base_path,
                                      f"{str(begin)}_{str(end)}",
                                      "_best_sim")

            detectors_out_df = pd.read_csv(os.path.join(exp_folder, 'detector.out.csv'), sep=";")
            # detectors_out_df = pd.read_csv(det_out_path, sep=";")
            pd_det_out = pd.concat([pd_det_out, detectors_out_df])

        import tempfile
        f = tempfile.NamedTemporaryFile()
        pd_det_out.to_csv(f, sep=';')

        shp_error_folder = os.path.join(base_path, 'SHP')
        if not os.path.exists(shp_error_folder):
            os.mkdir(shp_error_folder)

        # this should be fixed
        ErrorUtils.save_taz_errors_to_shp(net_path,
                                          taz_path,
                                          f.name,
                                          train_dataset_path,
                                          os.path.join(shp_error_folder, 'error.shp'))
