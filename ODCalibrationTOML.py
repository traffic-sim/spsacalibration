import argparse
import itertools
import os
import random
import shutil
import subprocess
import sys
import time

import numpy as np
import pandas as pd
import sumolib as sumolib
import xmltodict
from joblib.parallel import Parallel, delayed
from noisyopt.main import minimizeSPSA

import ErrorUtils
import GehUtil
from PathHandler import PathHandler, DestFolderType
from ResultMerger import assemble_results
import tomllib

if 'SUMO_HOME' in os.environ:
    sys.path.append(os.path.join(os.environ['SUMO_HOME'], 'tools'))
    sys.path.append(os.path.join(os.environ['SUMO_HOME'], 'tools', "route"))
    sys.path.append(os.path.join(os.environ['SUMO_HOME'], 'tools', "assign"))
    import randomTrips
    import route2OD
else:
    sys.exit("please declare environment variable 'SUMO_HOME'")


class ODCalibration:
    # THE FOLLOWING PARAMETERS ARE SET FROM THE JSON CONFIGURATION FILE
    # the full path to the SUMO network file
    net_path = None
    # the full path to the SUMO TAZ xml file
    taz_path = None
    # the scenario folder. This should contain 2 folders, 'train' and 'test'
    #scenario_folder = None
    # the path to the folder containing all the scenarios
    base_output_path = None
    # the full path to the .csv file containing the traffic count (train)
    src_train_count_dataset = None
    # the full path to the .csv file containing the traffic detectors (train)
    src_train_det_add_file = None
    # the full path to the .csv file containing the traffic count (test)
    src_test_count_dataset = None
    # the full path to the .csv file containing the traffic detectors (test)
    src_test_det_add_file = None
    # the frequency at which data is observed by detectors. This MUST match the same information in the scenario
    aggregation_frequency = 900
    # start time of the simulation
    sim_begin = 0
    # end time of the simulation
    sim_end = 900
    # A dictionary where KEY is a lane ID, VALUE is the TAZ containing the lane
    lanes_to_taz_mapping = None
    # A dictionary where KEY is a detector ID, VALUE is the lane containing the detector
    sensors_lanes_mapping = {}
    # the maximum number of vehicles that can be assigned to each OD pair
    max_num_vehicles_per_od = 10

    # ----------------------------------------------------------------------------------------------------
    # the number of iteration to perform with spsa
    SPSA_N_ITER = 1500
    # See randomTrips documentation
    random_trips_fringe_factor = 10
    random_trips_traffic_volume = 3000  # WARNING: this is used ONLY to create the OD pairs, not to regulate the amount of traffic
    random_trips_traffic_volume_type = "INSRATE"
    # optional edgedata file for randomtrips. This can be used to assign weights to edges before generating traffic
    edgedata_file = None
    # the parameter to use as a weights for edges when generating traffic with randomtrips
    edgedata_param = None

    od_pairs = None  # list of pairs of region IDs
    od_matrix = {}  # dictionary where KEY is a pair of region IDs, the value is the flow between the two regions

    # variable to keep track of the lowest score
    best_obj_fun_value = sys.maxsize
    # variable to keep track of the best
    best_param_vector = None

    path_handler: PathHandler = None  # a PathHandler object used to manage input/output path for the scenario
    calibration_iter: int = 0  # value used to keep track of the current calibration iteration

    @classmethod
    def do_random_trips(cls) -> None:
        """
        TODO: move to a SUMO module
        :return:
        """
        random.seed(time.time())
        trips_def_fname = cls.path_handler.get_random_trips_path()
        routes_def_fname = cls.path_handler.get_initial_randomtrips_routes()
        traffic_vol = None
        if type(cls.random_trips_traffic_volume) is float or type(cls.random_trips_traffic_volume) is int:
            traffic_vol = str(cls.random_trips_traffic_volume)
        elif type(cls.random_trips_traffic_volume) is list:
            traffic_vol = ','.join(list(map(lambda ir: str(ir), cls.random_trips_traffic_volume)))

        traffic_volume_prefix = "--period"
        if cls.random_trips_traffic_volume_type == "INSRATE":
            traffic_volume_prefix = "--insertion-rate"

        opts = ["-n", cls.net_path,
                f"{traffic_volume_prefix}={traffic_vol}",
                "-b", str(cls.sim_begin),
                "-e", str(cls.sim_end),
                "--random",
                "-o", trips_def_fname,
                "-r", routes_def_fname,
                "--validate",
                "--fringe-factor", str(cls.random_trips_fringe_factor),
                "--allow-fringe",
                ]

        if cls.edgedata_file is not None:
            opts.append("--edge-param")
            opts.append(cls.edgedata_param)
            opts.append("-a")
            opts.append(cls.edgedata_file)

        randomTrips.main(randomTrips.get_options(opts))

    @classmethod
    def routes2OD(cls) -> None:
        """
        TODO: move to a SUMO module
        :return:
        """
        opts = [
            "-r", cls.path_handler.get_random_trips_path(),
            "-a", cls.taz_path,
            "-o", cls.path_handler.get_od_pairs(),
            "-i", str(cls.aggregation_frequency),
        ]
        route2OD.main(route2OD.get_options(opts))

    @classmethod
    def OD2route(cls, od_pairs_fname: str, od_trips_fname: str, interval_id: str) -> None:
        """
        write OD to file
        :param od_pairs_fname: the full path to the xml file containing the od pairs in tazRelation format (see sumo doc)
        :param od_trips_fname: the full path to the XML file containing the trips (obtained using od2trips command)
        :param interval_id:
        :return:
        """
        with open(od_pairs_fname, 'w') as outf:
            sumolib.writeXMLHeader(outf, "$Id$", "data", "datamode_file.xsd")  # noqa
            outf.write(
                4 * ' ' + '<interval id="%s" begin="%s" end="%s">\n' % (interval_id, cls.sim_begin, cls.sim_end))
            for (src, dest), flow in cls.od_matrix.items():
                outf.write(8 * ' ' + '<tazRelation from="%s" to="%s" count="%s"/>\n' % (src, dest, flow))
            outf.write(4 * ' ' + '</interval>\n')
            outf.write('</data>\n')

        od2trips = sumolib.checkBinary("od2trips")
        opts = [
            f"{od2trips}",
            '-v', 'true',
            '--taz-files', cls.taz_path,
            '-z', od_pairs_fname,
            '-o', od_trips_fname,
            '--prefix', f"{str(cls.sim_begin)}_"
        ]

        print(f'command: {" ".join(opts)}')
        p = subprocess.Popen(opts)
        p.communicate()
        p.wait()

    @classmethod
    def do_traffic_assignment(cls, od_trips_fname, routes_fname):
        """
        TODO: move to a SUMO module

        :param od_trips_fname:
        :param routes_fname:
        :return:
        """
        marouter = sumolib.checkBinary("marouter")
        opts = [f"\"{marouter}\"",
                '-n', cls.net_path,
                '-r', od_trips_fname,
                '-o', routes_fname,
                '-b', str(cls.sim_begin),
                '-e', str(cls.sim_end),
                '-a', cls.taz_path,
                '--prefix', f"\"{str(cls.sim_begin)}_{str(cls.sim_end)}\""
                            ' --ignore-errors']
        output = os.popen(' '.join(opts)).read()


    @classmethod
    def do_simulation(cls, add_file=None):
        """
        TODO: move to a SUMO module
        :param add_file:
        :return:
        """
        sumo = sumolib.checkBinary("sumo")
        opts = [sumo,
                "-n", cls.net_path,
                "-r", cls.path_handler.get_calibrated_routes(),
                '-b', str(cls.sim_begin),
                '-e', str(cls.sim_end),
                '-a', add_file,
                "--no-step-log",
                "--ignore-route-errors",
                '--mesosim',
                "--no-warnings",
                "--meso-junction-control.limited", str(True),
                "--meso-jam-threshold", str(0.4)]
        print(f"sumo commands: {' '.join(opts)}")
        p = subprocess.Popen(opts)
        p.communicate()
        p.wait()

    @classmethod
    def parse_od_matrix(cls, fname) -> dict[(str, str), int]:
        """
        Parse the OD matrix (in tazRelation format) to dictionary.
        :param fname: the full path to the OD matrix
        :return: a dictionary where KEY is the OD pair, VALUE is the traffic flow
        """
        taz_relations = list(sumolib.xml.parse_fast(fname, 'tazRelation', ['from', 'to', 'count']))
        tazs = dict(map(lambda taz_dict: ((taz_dict.attr_from, taz_dict.to), int(taz_dict.count)), taz_relations))
        return tazs

    @classmethod
    def next_iter(cls) -> None:
        cls.calibration_iter += 1
        cls.path_handler.calibration_iter += 1

    @classmethod
    def set_time_horizon(cls, sim_begin, sim_end):
        cls.sim_begin = sim_begin
        cls.sim_end = sim_end

    @classmethod
    def set_iterations(cls, iterations):
        cls.SPSA_N_ITER = iterations

    @classmethod
    def create_filepath(cls) -> None:
        previous_folder = cls.path_handler.get_previous_iteration_folder()
        if os.path.exists(previous_folder):
            shutil.rmtree(previous_folder)

        if not os.path.exists(cls.base_output_path):
            os.makedirs(cls.base_output_path, exist_ok=True)

        output_folder = cls.path_handler.get_output_folder()
        if not os.path.exists(output_folder):
            os.makedirs(output_folder, exist_ok=True)
        det_add_file = cls.path_handler.get_sensors_add_file()
        if not os.path.exists(det_add_file):
            shutil.copyfile(cls.src_train_det_add_file, det_add_file)

    @classmethod
    def initialize_od(cls) -> None:
        """
        Read TAZ from file and return a dictionary (representing an OD matrix) where KEY is a pair of ID of TAZs and
        the value is 1.
        :return:
        """
        cls.od_matrix = dict(map(lambda polys: ((polys[0], polys[1]), 1), cls.od_pairs))

    @classmethod
    def save_error_to_file(cls, dataset, dest_folder_type: DestFolderType = DestFolderType.CALIBRATION):

        geh_path = cls.path_handler.get_geh_output_file(dest_folder_type)
        detectors_out_csv = cls.path_handler.get_detectors_out_csv(dest_folder_type)
        detectors_out_xlsx = cls.path_handler.get_detectors_out_xlsx(dest_folder_type)
        traffic_comparison_xlsx = cls.path_handler.get_traffic_comparison_xlsx(dest_folder_type)

        geh_df, _ = GehUtil.geh_to_file(geh_path, detectors_out_csv, dataset)
        ErrorUtils.detectors_output_to_excel(detectors_out_csv, detectors_out_xlsx, dataset)
        real_counts, sim_counts_df = ErrorUtils.get_real_and_sim_data(detectors_out_csv, dataset)

        if dest_folder_type == DestFolderType.BEST_SIM_TEST:
            sheet_name = "Average Traffic (Test set)"
            writer = pd.ExcelWriter(traffic_comparison_xlsx, engine='openpyxl', mode='a')
        else:
            sheet_name = "Average Traffic (Train set)"
            writer = pd.ExcelWriter(traffic_comparison_xlsx, mode="w")
        ErrorUtils.traffic_counts_sum_to_excel(real_counts, sim_counts_df).to_excel(writer, sheet_name=sheet_name)
        writer.close()

    @classmethod
    def calculate_loss(cls, to_file=False):
        # return cls.calculate_geh_loss_fun(to_file)
        # return cls.calculate_mae_loss_fun(to_file)
        return cls.region_avg_err_loss_fun(to_file)

    @classmethod
    def calculate_geh_loss_fun(cls, to_file=False):
        geh_df, _ = GehUtil.geh_to_file(cls.path_handler.get_geh_output_file(),
                                        cls.path_handler.get_detectors_out_csv(),
                                        cls.src_train_count_dataset)

        num_rows = geh_df.shape[0]
        loss = 0
        for i in range(num_rows):
            the_row = geh_df.iloc[i, :]
            valid_geh = list(filter(lambda geh_val: geh_val < 200, the_row.to_list()))
            loss += np.sum(valid_geh)
        if to_file:
            with open(cls.path_handler.get_objective_fun_csv_fname(), 'a') as f:
                f.write("{}".format(loss))
                f.write("\n")  # Next line.
        return loss

    @classmethod
    def calculate_mae_loss_fun(cls, to_file=False):
        loss = GehUtil.calculate_traffic_volume_difference(cls.path_handler.get_detectors_out_csv(),
                                                           cls.src_train_count_dataset)

        if to_file:
            with open(cls.path_handler.get_objective_fun_csv_fname(), 'a') as f:
                f.write("{}".format(loss))
                f.write("\n")  # Next line.
        return loss

    @classmethod
    def region_avg_err_loss_fun(cls, to_file=False):
        real_counts_df, sim_counts_df = ErrorUtils.get_real_and_sim_data(cls.path_handler.get_detectors_out_csv(),
                                                                         cls.src_train_count_dataset)
        mae_df = ErrorUtils.compare_sim_and_real_counts(real_counts_df, sim_counts_df, "MAE")
        # dict where KEY is an EDGE ID, value is the MAE
        mae_dict = mae_df.to_dict()[0]
        mae_by_regions = {}

        if len(cls.sensors_lanes_mapping) > 0:
            mae_dict = {cls.sensors_lanes_mapping[k]: v for k, v in mae_dict.items() if k in cls.sensors_lanes_mapping}

        for edge_id, error in mae_dict.items():
            # TODO warning: in BXL scenario we have LANE ID==Sensor ID, in Namur we have EDGE ID==Sensor ID
            if edge_id not in cls.lanes_to_taz_mapping:
                continue
            dest_region = cls.lanes_to_taz_mapping[edge_id]
            if dest_region not in mae_by_regions:
                mae_by_regions[dest_region] = []
            mae_by_regions[dest_region].append(mae_dict[edge_id])
        mae_by_regions_out = {k: np.mean(v) for k, v in mae_by_regions.items()}

        loss = np.sum(list(mae_by_regions_out.values()))
        print('LOSS: {}'.format(loss))
        if to_file:
            with open(cls.path_handler.get_objective_fun_csv_fname(), 'a') as f:
                f.write("{}".format(loss))
                f.write("\n")  # Next line.
        return loss

    @classmethod
    def assign_od(cls, flow_assignment):
        for idx, od_pair in enumerate(cls.od_pairs):
            cls.od_matrix[od_pair] = flow_assignment[idx]

    @classmethod
    def set_od_pairs(cls, od_pairs):
        cls.od_pairs = list(od_pairs.keys())

    @classmethod
    def run_best_simulation(cls, x):
        """
        This is invoked AFTER the calibration. THis is to execute a simulation with the routes obtained after the
        calibration. The results are stored into a folder named "_best_sim".
        :param x: the OD pairs values obtained after the calibration process
        :return:
        """
        output_folder = cls.path_handler.get_best_sim_output_folder()
        if not os.path.exists(output_folder):
            os.makedirs(output_folder)

        det_add_file = cls.path_handler.get_sensors_add_file(DestFolderType.BEST_SIM)
        det_out_file = cls.path_handler.get_detectors_out_xml(DestFolderType.BEST_SIM)

        if not os.path.exists(det_add_file):
            shutil.copyfile(cls.src_train_det_add_file, det_add_file)

        cls.assign_od(x)  # assign the values to the OD pairs
        od_pairs = ODCalibration.path_handler.get_od_pairs()
        od_trips = ODCalibration.path_handler.get_od_trips(dest_folder_type=DestFolderType.BEST_SIM)
        routes = cls.path_handler.get_calibrated_routes(DestFolderType.BEST_SIM)

        cls.OD2route(od_pairs, od_trips, "DEFAULT_VEHTYPE")  # -> generate trips based on OD matrix
        cls.do_traffic_assignment(od_trips, routes)  # -> from trips to paths
        cls.do_simulation(det_add_file)  # -> simulate traffic

        os.system(f"{sys.executable} \"{os.environ['SUMO_HOME']}/tools/xml/xml2csv.py\" {det_out_file}")
        cls.save_error_to_file(cls.src_train_count_dataset,
                               dest_folder_type=DestFolderType.BEST_SIM)  # -> save the error and GEH to file

        # TEST SENSORS------------------------------------------------------
        # test folder is for the output related to the test/validation sensors set
        test_folder = cls.path_handler.get_best_sim_test_folder()
        if not os.path.exists(test_folder):
            os.makedirs(test_folder, exist_ok=True)

        test_add_file = cls.path_handler.get_sensors_add_file(DestFolderType.BEST_SIM_TEST)
        shutil.copyfile(cls.src_test_det_add_file, test_add_file)  # copy to dest TEST folder
        cls.do_simulation(test_add_file)  # -> simulate traffic (use test detectors)
        test_det_out_file = cls.path_handler.get_detectors_out_xml(dest_folder_type=DestFolderType.BEST_SIM_TEST)
        os.system(f"{sys.executable} \"{os.environ['SUMO_HOME']}/tools/xml/xml2csv.py\" {test_det_out_file}")

        cls.save_error_to_file(cls.src_test_count_dataset,
                               dest_folder_type=DestFolderType.BEST_SIM_TEST)  # -> save the error and GEH to file

    @classmethod
    def get_number_variables(cls):
        """
        This is to calculate the number of variable (each variable is an OD pair). I first use randomTrips, then
        evaluate the OD based on the generated paths.

        :return: the number of OD pairs
        """
        cls.create_filepath()
        if cls.od_pairs is None:
            # 1. generate random paths between the TAZs
            cls.do_random_trips()
            # 2. find the pairs of regions from the random trips
            cls.routes2OD()
            # 3. set ODCalibration.setODPairs(...) with the found pairs of TAZ IDs
            cls.set_od_pairs(cls.parse_od_matrix(cls.path_handler.get_od_pairs()))
        return len(cls.od_pairs)


# put seed in params (no qa, required by minimizeSPSA apparently)
def optimize_traffic_iter(flow_assignment: list[float], seed=42):
    """
    Main point of the calibration method. This is invoked by SPSA at each iteration

    :param flow_assignment:
    :param seed:
    :return:
    """
    ODCalibration.next_iter()  # prepare the next calibration_iter
    ODCalibration.create_filepath()  # prepare the file path
    ODCalibration.initialize_od()  # initialize the OD matrix (all values to 1)
    ODCalibration.assign_od(flow_assignment)  # then, assign the values to the OD pairs (regulated by SPSA)

    od_pairs = ODCalibration.path_handler.get_od_pairs()
    od_trips = ODCalibration.path_handler.get_od_trips()
    routes = ODCalibration.path_handler.get_calibrated_routes()

    ODCalibration.OD2route(od_pairs, od_trips, "DEFAULT_VEHTYPE")  # -> generate trips based on OD matrix
    ODCalibration.do_traffic_assignment(od_trips, routes)  # -> do traffic assignment (from trips to paths)
    ODCalibration.do_simulation(ODCalibration.path_handler.get_sensors_add_file())  # -> simulate traffic

    det_out_file = ODCalibration.path_handler.get_detectors_out_xml()
    os.system(f"{sys.executable} \"{os.environ['SUMO_HOME']}/tools/xml/xml2csv.py\" {det_out_file}")

    ODCalibration.save_error_to_file(ODCalibration.src_train_count_dataset)  # -> save the error and GEH to file
    loss = ODCalibration.calculate_loss(to_file=True)

    if loss < ODCalibration.best_obj_fun_value:
        ODCalibration.best_obj_fun_value = loss
        ODCalibration.best_param_vector = flow_assignment
    return loss


def parallel_calibration_aux(sim_begin: int, sim_end: int, initial_traffic_amount: float, data):
    """
    Perform traffic calibration for the interval sim_begin->sim_end
    :param sim_begin:
    :param sim_end:
    :param initial_traffic_amount: the initial amount of traffic that flows between all OD pairs
    :param data:
    :return:
    """
    train_scenario_dir = data['dataset']['train_scenario_dir']
    test_scenario_dir = data['dataset']['test_scenario_dir']

    ODCalibration.base_output_path = data['dataset']['output_simulation_dir']
    ODCalibration.net_path = data['net_path']
    ODCalibration.taz_path = data['taz_path']
    ODCalibration.max_num_vehicles_per_od = data['spsa']['max_num_vehicles_per_od']

    ODCalibration.src_train_count_dataset = os.path.join(train_scenario_dir, "count_dataset.csv")
    ODCalibration.src_train_det_add_file = os.path.join(train_scenario_dir, "det.add.xml")
    ODCalibration.src_test_count_dataset = os.path.join(test_scenario_dir, "count_dataset.csv")
    ODCalibration.src_test_det_add_file = os.path.join(test_scenario_dir, "det.add.xml")

    train_sensors_edges_mapping_fname = os.path.join(train_scenario_dir, "sensors_lanes_mapping.csv")
    test_sensors_edges_mapping_fname = os.path.join(test_scenario_dir, "sensors_lanes_mapping.csv")

    if os.path.exists(train_sensors_edges_mapping_fname):
        ODCalibration.sensors_lanes_mapping = \
            pd.read_csv(train_sensors_edges_mapping_fname, sep=";").set_index("camera_id").to_dict()['lane_id']
    if os.path.exists(test_sensors_edges_mapping_fname):
        ODCalibration.sensors_lanes_mapping.update(
            pd.read_csv(test_sensors_edges_mapping_fname, sep=";").set_index("camera_id").to_dict()['lane_id'])

    lanes_to_taz_mapping, _ = ErrorUtils.get_lanes_per_taz(ODCalibration.taz_path, ODCalibration.net_path)
    ODCalibration.lanes_to_taz_mapping = lanes_to_taz_mapping

    ODCalibration.set_time_horizon(sim_begin, sim_end)
    ODCalibration.path_handler = PathHandler(data['dataset']['output_simulation_dir'],
                                             sim_begin,
                                             sim_end,
                                             ODCalibration.calibration_iter)
    ODCalibration.set_iterations(data['spsa']['calibration_iterations'])

    num_vars = ODCalibration.get_number_variables()
    bounds = [(0.0, ODCalibration.max_num_vehicles_per_od) for _ in range(num_vars)]
    initial_flow_assignment = (initial_traffic_amount / num_vars) * np.ones((num_vars,), dtype=float)
    minimizeSPSA(optimize_traffic_iter, niter=ODCalibration.SPSA_N_ITER, x0=initial_flow_assignment, bounds=bounds,
                 c=30, a=1)
    ODCalibration.run_best_simulation(ODCalibration.best_param_vector)


def parallel_calibration(data):
    sim_begin = data['simulation']['start_time']
    sim_end = data['simulation']['end_time']
    aggr_freq = data['dataset']['aggregation_frequency']
    train_scenario_path = data['dataset']['train_scenario_dir']

    real_counts_df = ErrorUtils.load_real_traffic_count(
        os.path.join(train_scenario_path, "count_dataset.csv"), True)
    ts = list(range(sim_begin, sim_end + 1, aggr_freq))
    a, b = itertools.tee(ts)
    next(b, None)
    params = list(map(lambda interval: (interval[0], interval[1], real_counts_df.loc[interval[0], :].sum(), data),
                      list(zip(a, b))))

    Parallel(n_jobs=1)(delayed(parallel_calibration_aux)(*x) for x in params)
    #for p in params:
    #    parallel_calibration_aux(*p)


def main_with_args(toml_config_file="callibrator_conf.toml"):
    with open(toml_config_file, "rb") as f:
        data = tomllib.load(f)

    train_scenario_dir = data['dataset']['train_scenario_dir']
    test_scenario_dir = data['dataset']['test_scenario_dir']

    sim_begin = data['simulation']['start_time']
    sim_end = data['simulation']['end_time']
    aggr_freq = data['dataset']['aggregation_frequency']
    parallel_calibration(data)
    assemble_results(data['dataset']['output_simulation_dir'],
                     data['net_path'],
                     sim_begin, sim_end, aggr_freq,
                     os.path.join(train_scenario_dir, "count_dataset.csv"),
                     os.path.join(test_scenario_dir, "count_dataset.csv"),
                     taz_path=data['taz_path'])


if __name__ == '__main__':
    """
    Example:
    
    python ODCalibrationTOML.py conf.toml
    """
    main_with_args(sys.argv[1])
