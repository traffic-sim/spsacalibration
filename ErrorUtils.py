import os
import sys
from xml.sax import parse

import geopandas
import numpy as np
import pandas as pd
import sumolib
from shapely.geometry.polygon import Polygon
from sklearn.metrics import mean_absolute_percentage_error, mean_absolute_error, root_mean_squared_error

if 'SUMO_HOME' in os.environ:
    sys.path.append(os.path.join(os.environ['SUMO_HOME'], 'tools'))
    import edgesInDistricts
    from edgesInDistricts import DistrictEdgeComputer
else:
    sys.exit("please declare environment variable 'SUMO_HOME'")


def load_real_traffic_count(fname: str, convert_ts_to_int=False) -> pd.DataFrame:
    """

    :param fname: the PATH to the traffic count dataset (table)
    :param convert_ts_to_int: if TRUE, then the timestamp are converted to seconds
    :return: a DATAFRAME containing the data in tabular form
    """
    real_counts = pd.read_csv(fname, sep=";", index_col="ts", parse_dates=True)

    if convert_ts_to_int:
        ts = list(real_counts.index.unique())
        ts_sumo = [(ts_other - ts[0]).total_seconds() for ts_other in ts]
        real_counts['ts_sumo'] = ts_sumo
        real_counts.set_index('ts_sumo', inplace=True)
    return real_counts


def detectors_out_to_table(real_data_df, sim_data_df, field_name) -> pd.DataFrame:
    list_osm_edges = sim_data_df['interval_id'].unique()
    detectors_name = set(list_osm_edges).intersection(set(real_data_df.columns))
    data_dict = {}
    for osm_id in detectors_name:
        data = sim_data_df.loc[sim_data_df['interval_id'] == osm_id][field_name]
        data_dict[osm_id] = data.to_numpy()
    data_df_formatted = pd.DataFrame.from_dict(data_dict)
    return data_df_formatted


def get_sim_data(detectors_csv_out, field="interval_entered") -> pd.DataFrame:
    """
    Read the traffic counts collected by sensors *in the simulation*. This is provided by detectors output in SUMO.
    :param detectors_csv_out: the output of the detectors in CSV format (converted using $SUMO_HOME/tools/xml/xml2csv)
    :param field: the field to extract, by default is the number of vehicles entered
    :return:
    """
    sim_data_df = pd.read_csv(detectors_csv_out, sep=";")
    # take the list of unique edges in the csv file
    list_osm_edges = sim_data_df['interval_id'].unique()
    data_dict = {}
    # now organize the data from each sensor in one column
    for osm_id in list_osm_edges:
        data = sim_data_df.loc[sim_data_df['interval_id'] == osm_id][field]
        data_dict[osm_id] = data.to_numpy()
    # convert the dict to dataframe
    return pd.DataFrame.from_dict(data_dict)


def get_real_and_sim_data(detectors_csv_out: str, traffic_counts_fname: str) -> (pd.DataFrame, pd.DataFrame):
    """
    real data   -> input traffic count
    sim data    -> field 'entered' from virtual sensors output
    :param detectors_csv_out: the PATH to SUMO detectors output file
    :param traffic_counts_fname: the PATH to the CSV file containing the traffic count in table form
    :return:
    """
    sim_counts = pd.read_csv(detectors_csv_out, sep=";")
    real_counts_df = load_real_traffic_count(traffic_counts_fname, True)
    real_counts_df = real_counts_df.loc[sim_counts.interval_begin.unique().tolist()]
    sim_counts_df_begin = detectors_out_to_table(real_counts_df, sim_counts, 'interval_entered')
    return real_counts_df, sim_counts_df_begin


def traffic_counts_sum_to_excel(df_real, df_sim):
    df_avg = pd.DataFrame()
    df_avg['real'] = df_real.sum(axis=1)

    avg_sim_df = df_sim.copy(deep=True)
    avg_sim_df['idx'] = df_real.index.to_list()
    avg_sim_df.set_index('idx', inplace=True)
    avg_sim_df = avg_sim_df.sum(axis=1)

    df_avg['sim'] = avg_sim_df
    return df_avg


def compare_sim_and_real_counts(df_real: pd.DataFrame, df_sim: pd.DataFrame, error_fun: str = None) -> pd.DataFrame:
    """
    Compare the two input dataset according to the input error function. If no error function is provided, then the
    output is the signed difference real-sim
    :param df_real: the DATAFRAME containing the real data
    :param df_sim: the DATAFRAME containing the simulated data
    :param error_fun: the error function
    :return: a DATAFRAME
    """
    err_dict = {}
    for col in df_sim.columns:
        if col not in df_real.columns:
            continue
        real_c = df_real[col].to_numpy()
        sim_c = df_sim[col].to_numpy()

        if error_fun is None:
            err_dict[col] = real_c - sim_c
        else:
            with np.errstate(divide='ignore', invalid='ignore'):
                if error_fun == "MAPE":
                    err_dict[col] = mean_absolute_percentage_error(real_c, sim_c)
                elif error_fun == "MAE":
                    err_dict[col] = mean_absolute_error(real_c, sim_c)
                elif error_fun == "RMSE":
                    err_dict[col] = root_mean_squared_error(real_c, sim_c)

    if error_fun is None:
        return pd.DataFrame.from_dict(err_dict)
    return pd.DataFrame.from_dict(err_dict, orient="index")


def detectors_output_to_excel(detectors_csv_out, dest_fname, real_data_counts_fname):
    real_counts, sim_counts_df = get_real_and_sim_data(detectors_csv_out, real_data_counts_fname)

    err_df = compare_sim_and_real_counts(real_counts, sim_counts_df)
    mape_df = compare_sim_and_real_counts(real_counts, sim_counts_df, "MAPE")
    mae_df = compare_sim_and_real_counts(real_counts, sim_counts_df, "MAE")
    rmse_df = compare_sim_and_real_counts(real_counts, sim_counts_df, "RMSE")
    df_errors_resume = pd.concat([mape_df, mae_df, rmse_df], axis=1)
    df_errors_resume.columns = ["MAPE", "MAE", "RMSE"]

    writer = pd.ExcelWriter(dest_fname, mode="w")
    sim_counts = pd.read_csv(detectors_csv_out, sep=";")
    err_df['ts_sumo'] = list(sim_counts.interval_begin.unique())
    err_df.set_index('ts_sumo', inplace=True)
    err_df.to_excel(writer, sheet_name="Real-Simulated")

    sim_counts_df['ts_sumo'] = list(sim_counts.interval_begin.unique())
    sim_counts_df.set_index('ts_sumo', inplace=True)
    sim_counts_df.to_excel(writer, sheet_name="Simulated")

    df_errors_resume.to_excel(writer, sheet_name="Errors")
    writer.close()


def get_lanes_per_taz(taz_fname, net_path) -> (dict[str, str], dict[str, Polygon]):
    """
    Read the taz and the network files in input and return a mapping (dictionary) between TAZ and edges.
    :param taz_fname: PATH to taz file
    :param net_path:  PATH to road network file
    :return: a tuple: the first element is a dictionary where KEY is the ID of the TAZ, VALUE is a list of edges within the taz, the second a dictionary where the KEY is the taz ID, the value is a Polygon object
    """
    net = sumolib.net.readNet(net_path)
    reader = DistrictEdgeComputer(net)
    options = edgesInDistricts.parse_args(args=['-n', net_path])[0]
    poly_reader = sumolib.shapes.polygon.PolygonReader(True)
    parse(taz_fname, poly_reader)
    reader.computeWithin(poly_reader.getPolygons(), options)

    # key -> Edge object, VALUE -> Polygon object
    the_map = reader.getEdgeDistrictMap()
    the_tazs = list(set(the_map.values()))

    taz_polygon_dict = {}
    for taz in the_tazs:
        taz_polygon_dict[taz.id] = Polygon([net.convertXY2LonLat(p[0], p[1]) for p in taz.shape])

    edge_taz_dict = {}
    for edge, poly in the_map.items():
        #for lane in edge.getLanes():
        #    edge_taz_dict[lane.getID()] = poly.id
        edge_taz_dict[edge.getID()] = poly.id
    return edge_taz_dict, taz_polygon_dict


def save_regions_to_shapefile(taz_poly_dict, taz_err_df, shapefile_name):
    gdf = geopandas.GeoDataFrame.from_dict(taz_poly_dict, orient="index")
    if taz_err_df is not None:
        regions_df = pd.DataFrame.from_dict(taz_poly_dict, orient="index")
        gdf = geopandas.GeoDataFrame(regions_df.merge(taz_err_df, left_index=True, right_index=True))
    gdf.rename(columns={0: "geometry"}, inplace=True)
    gdf = gdf.set_geometry("geometry")
    gdf.to_file(shapefile_name)


def save_taz_errors_to_shp(net_path, taz_path, detectors_csv_out, real_data_counts_fname, dest_shp_fname):
    real_counts, sim_counts_df = get_real_and_sim_data(detectors_csv_out, real_data_counts_fname)
    mape_df = compare_sim_and_real_counts(real_counts, sim_counts_df, "MAPE")
    mae_df = compare_sim_and_real_counts(real_counts, sim_counts_df, "MAE")
    rmse_df = compare_sim_and_real_counts(real_counts, sim_counts_df, "RMSE")

    lanes_to_taz_mapping, geo_ref_taz = get_lanes_per_taz(taz_path, net_path)

    mape_dict = mape_df.to_dict()[0]
    mae_dict = mae_df.to_dict()[0]
    rmse_dict = rmse_df.to_dict()[0]

    mape_by_regions = {}
    mae_by_regions = {}
    rmse_by_regions = {}
    for lane_id, error in mape_dict.items():
        if lane_id not in lanes_to_taz_mapping:
            continue
        dest_region = lanes_to_taz_mapping[lane_id]
        if dest_region not in mape_by_regions:
            mape_by_regions[dest_region] = []
            mae_by_regions[dest_region] = []
            rmse_by_regions[dest_region] = []
        mape_by_regions[dest_region].append(error)
        mae_by_regions[dest_region].append(mae_dict[lane_id])
        rmse_by_regions[dest_region].append(rmse_dict[lane_id])

    mae_by_regions_out = {k: np.mean(v) for k, v in mae_by_regions.items()}
    mape_by_regions_out = {k: np.mean(v) for k, v in mape_by_regions.items()}
    rmse_by_regions_out = {k: np.mean(v) for k, v in rmse_by_regions.items()}

    mae_df = pd.DataFrame.from_dict(mae_by_regions_out, orient="index", columns=['MAE'])
    mape_df = pd.DataFrame.from_dict(mape_by_regions_out, orient="index", columns=['MAPE'])
    rmse_df = pd.DataFrame.from_dict(rmse_by_regions_out, orient="index", columns=['RMSE'])
    err_df = pd.concat([mae_df, mape_df, rmse_df], axis=1)
    save_regions_to_shapefile(geo_ref_taz, err_df, dest_shp_fname)
