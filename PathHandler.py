import os
from enum import Enum


class DestFolderType(Enum):
    CALIBRATION = 1
    BEST_SIM = 2
    BEST_SIM_TEST = 3


class PathHandler:

    def __init__(self, base_path, sim_begin, sim_end, calibration_iter):
        self.base_path = base_path
        self.sim_begin = sim_begin
        self.sim_end = sim_end
        self.calibration_iter = calibration_iter

    def get_previous_iteration_folder(self, back_steps=10):
        return os.path.join(self.base_path,
                            f"{str(self.sim_begin)}_{str(self.sim_end)}",
                            f"output_iter_{self.calibration_iter - back_steps}/")

    def get_output_folder(self):
        """
        The folder where the output of the current calibration iteration are saved
        :return:
        """
        return os.path.join(self.base_path,
                            f"{str(self.sim_begin)}_{str(self.sim_end)}{os.sep}output_iter_{self.calibration_iter}/")

    def get_best_sim_output_folder(self):
        return os.path.join(self.base_path, f"{str(self.sim_begin)}_{str(self.sim_end)}", "_best_sim")

    def get_best_sim_test_folder(self):
        return os.path.join(self.get_best_sim_output_folder(), "test")

    def get_detectors_out_xml(self, dest_folder_type: DestFolderType = DestFolderType.CALIBRATION):
        match dest_folder_type:
            case DestFolderType.CALIBRATION:
                return os.path.join(self.get_output_folder(), "detector.out.xml")
            case DestFolderType.BEST_SIM:
                return os.path.join(self.get_best_sim_output_folder(), "detector.out.xml")
            case DestFolderType.BEST_SIM_TEST:
                return os.path.join(self.get_best_sim_test_folder(), "detector.out.xml")

    def get_sensors_add_file(self, dest_folder_type: DestFolderType = DestFolderType.CALIBRATION):
        """
        The additional detector file for the current calibration iteration
        :param dest_folder_type:
        :return:
        """
        match dest_folder_type:
            case DestFolderType.CALIBRATION:
                return os.path.join(self.get_output_folder(), "det.add.xml")
            case DestFolderType.BEST_SIM:
                return os.path.join(self.get_best_sim_output_folder(), "det.add.xml")
            case DestFolderType.BEST_SIM_TEST:
                return os.path.join(self.get_best_sim_test_folder(), "det.add.xml")

    def get_geh_output_file(self, dest_folder_type: DestFolderType = DestFolderType.CALIBRATION):
        geh_fname = "geh.train.xlsx"
        if dest_folder_type == DestFolderType.BEST_SIM_TEST:
            geh_fname = "geh.test.xlsx"
        match dest_folder_type:
            case DestFolderType.CALIBRATION:
                return os.path.join(self.get_output_folder(), geh_fname)
            case DestFolderType.BEST_SIM:
                return os.path.join(self.get_best_sim_output_folder(), geh_fname)
            case DestFolderType.BEST_SIM_TEST:
                return os.path.join(self.get_best_sim_output_folder(), geh_fname)

    def get_detectors_out_csv(self, dest_folder_type: DestFolderType = DestFolderType.CALIBRATION):
        match dest_folder_type:
            case DestFolderType.CALIBRATION:
                return os.path.join(self.get_output_folder(), "detector.out.csv")
            case DestFolderType.BEST_SIM:
                return os.path.join(self.get_best_sim_output_folder(), "detector.out.csv")
            case DestFolderType.BEST_SIM_TEST:
                return os.path.join(self.get_best_sim_test_folder(), "detector.out.csv")

    def get_detectors_out_xlsx(self, dest_folder_type: DestFolderType = DestFolderType.CALIBRATION):
        match dest_folder_type:
            case DestFolderType.CALIBRATION:
                return os.path.join(self.get_output_folder(), "detector.out.xlsx")
            case DestFolderType.BEST_SIM:
                return os.path.join(self.get_best_sim_output_folder(), "detector.out.xlsx")
            case DestFolderType.BEST_SIM_TEST:
                return os.path.join(self.get_best_sim_test_folder(), "detector.out.xlsx")

    def get_traffic_comparison_xlsx(self, dest_folder_type: DestFolderType = DestFolderType.CALIBRATION):
        match dest_folder_type:
            case DestFolderType.CALIBRATION:
                return os.path.join(self.get_output_folder(), "real_vs_sim_comparison.xlsx")
            case DestFolderType.BEST_SIM:
                return os.path.join(self.get_best_sim_output_folder(), "real_vs_sim_comparison.xlsx")
            case DestFolderType.BEST_SIM_TEST:
                return os.path.join(self.get_best_sim_output_folder(), "real_vs_sim_comparison.xlsx")

    def get_od_pairs(self, dest_folder_type: DestFolderType = DestFolderType.CALIBRATION):
        match dest_folder_type:
            case DestFolderType.CALIBRATION:
                return os.path.join(self.get_output_folder(), "od.pairs.xml")
            case DestFolderType.BEST_SIM:
                return os.path.join(self.get_output_folder(), "od.pairs.xml")
            case DestFolderType.BEST_SIM_TEST:
                return os.path.join(self.get_best_sim_output_folder(), "od.pairs.xml")

    def get_od_trips(self, dest_folder_type: DestFolderType = DestFolderType.CALIBRATION):
        match dest_folder_type:
            case DestFolderType.CALIBRATION:
                return os.path.join(self.get_output_folder(), "od_trips.xml")
            case DestFolderType.BEST_SIM:
                return os.path.join(self.get_best_sim_output_folder(), "od_trips.xml")
            case DestFolderType.BEST_SIM_TEST:
                return os.path.join(self.get_best_sim_output_folder(), "od_trips.xml")

    def get_calibrated_routes(self, dest_folder_type: DestFolderType = DestFolderType.CALIBRATION):
        match dest_folder_type:
            case DestFolderType.CALIBRATION:
                return os.path.join(self.get_output_folder(), "routes.dua.xml")
            case DestFolderType.BEST_SIM:
                return os.path.join(self.get_best_sim_output_folder(), "routes.dua.xml")
            case DestFolderType.BEST_SIM_TEST:
                return os.path.join(self.get_best_sim_output_folder(), "routes.dua.xml")

    def get_objective_fun_csv_fname(self):
        return os.path.join(self.base_path, f"{str(self.sim_begin)}_{str(self.sim_end)}",
                            'obj_function.csv')

    def get_initial_randomtrips_routes(self):
        return os.path.join(self.get_output_folder(), "initial_routes.xml")

    def get_random_trips_path(self):
        return os.path.join(self.get_output_folder(), f"trips.trips.xml")
