import json
import os
import subprocess
import sys
import ODCalibration


def invoke_calibrator(the_args):
    p1 = subprocess.Popen(the_args)
    p1.communicate()
    p1.wait()


def load_configuration(fname):
    with open(fname) as f:
        return json.load(f)


if __name__ == '__main__':
    """
        PLEASE NOTE: the first positional argument of the program must be a JSON configuration file
        Examples are in the root folder of this project
    """
    config_dict = load_configuration(sys.argv[1])
    base_scenarios_path = config_dict['scenario_path']
    base_output_path = config_dict['output_path']
    net_path = config_dict['net_path']
    if net_path.startswith('$'):
        net_path = os.environ[net_path.strip('$')]

    all_taz_config = {}
    for taz_config in config_dict['taz_config']:
        pickle_path = taz_config["taz_pickle_path"] if "taz_pickle_path" in taz_config else ''
        all_taz_config[taz_config['config_name']] = (taz_config["taz_polygon_xml"], pickle_path)
    sim_begin = config_dict['sim_begin_time']
    sim_end = config_dict['sim_end_time']
    aggr_frequency = config_dict['data_aggr_frequency']

    # ------------------------------------------------------------------------------------------------------------------
    scenario_list = [f for f in os.listdir(base_scenarios_path) if
                     not os.path.isfile(os.path.join(base_scenarios_path, f))]

    calibration_commands = []
    for scenario in scenario_list:
        scenario_path = os.path.join(base_scenarios_path, scenario)
        for taz_config_id, taz_config in all_taz_config.items():
            experiment_output_path = os.path.join(base_output_path, scenario, taz_config_id)
            args = ["python", "ODCalibration.py",
                    "-n", net_path,
                    "-t", taz_config[0],
                    "-s", scenario_path,
                    "-o", experiment_output_path,
                    "-b", str(sim_begin),
                    "-e", str(sim_end),
                    "-f", str(aggr_frequency)]
            print(f"{' '.join(args)}")
            ODCalibration.main_with_args(args[2:])
